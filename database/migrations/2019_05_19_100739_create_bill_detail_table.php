<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bill')->unsigned();
            $table->integer('id_product')->unsigned();
            $table->integer('id_subcategory')->unsigned();
            $table->integer('quantity');
            $table->double('unit_price');
            $table->integer('promotion_price');
            $table->timestamps();
            //$table->foreign('id_bill')->references('id')->on('bill');
            //$table->foreign('id_product')->references('id')->on('product');
            //$table->foreign('id_subcategory')->references('id')->on('subcategory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_detail');
    }
}
