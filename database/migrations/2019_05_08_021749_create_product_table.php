<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('id_type')->unsigned();
            $table->integer('id_subcategory')->unsigned();
            $table->text('short_description');
            $table->text('long_description');
            $table->double('unit_price')->comment('đơn giá');
            $table->double('promotion_price')->comment('giá khuyến mãi');
            $table->string('image')->default(0);
            $table->string('image1')->default(0);
            $table->string('image2')->default(0);
            $table->string('image3')->default(0);
            $table->boolean('top');
            $table->timestamps();
            //$table->foreign('id_type')->references('id')->on('type_product');
            //$table->foreign('id_subcategory')->references('id')->on('subcategory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
