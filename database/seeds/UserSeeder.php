<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'Quỳnh Trang', 'email' => 'quynhtrang@gmail.com', 'gender' => 0, 'password' => '$2y$10$wKryRucMQtQWIUHq8O.PxeOEtRrIIZnliJpnQcg8Fu6iu903Z.snm', 'phone' => '0964123123', 'address' => 'Hà Nội', 'role' => 1],
            ['name' => 'Thu Thủy', 'email' => 'thuthuy@gmail.com', 'gender' => 0, 'password' => '$2y$10$wKryRucMQtQWIUHq8O.PxeOEtRrIIZnliJpnQcg8Fu6iu903Z.snm', 'phone' => '0964123123', 'address' => 'Hà Nội', 'role' => 0]
        ]);
    }
}
