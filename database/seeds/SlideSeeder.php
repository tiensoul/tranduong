<?php

use Illuminate\Database\Seeder;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slide')->insert([
            ['link' => 'uploads/banner/slider_1.jpg', 'image' => 'slider_1.jpg'],
            ['link' => 'uploads/banner/banner-danh-muc-trang-diem-01.png', 'image' => 'banner-danh-muc-trang-diem-01.png'],
            ['link' => 'uploads/banner/5659b7f3d8bcf3f.jpg', 'image' => '5659b7f3d8bcf3f.jpg'],
            ['link' => 'uploads/banner/banner-son-ciciro-1.jpg', 'image' => 'banner-son-ciciro-1.jpg'],
            ['link' => 'uploads/banner/846018251715.jpg', 'image' => '846018251715.jpg'],
            ['link' => 'uploads/banner/56599fba487e47d.jpg', 'image' => '56599fba487e47d.JPG'],
            ['link' => 'uploads/banner/41583afb0536422.jpg', 'image' => '41583afb0536422.jpg'],
            ['link' => 'uploads/banner/banner_line2.png', 'image' => 'banner_line2.png']
        ]);
    }
}
