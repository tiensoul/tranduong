@extends('pages/master')
@section('content')
</div>
<div class="container" style="margin-top: 100px;">
    <div class="row">




        <div class="site-content col-sm-12" role="main">

            <article id="post-22307" class="post-22307 page type-page status-publish">

                <div class="entry-content">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="col-lg-8">

                            <form class="woocommerce-cart-form" action="{{ route('giohang') }}" method="post">


                                <div class="responsive-table">
                                    <table class="shop_table  cart woocommerce-cart-form__contents" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th class="product-remove">&nbsp;</th>
                                                <th class="product-thumbnail">&nbsp;</th>
                                                <th class="product-name">Sản phẩm</th>
                                                <th class="product-price">Giá</th>
                                                <th class="product-quantity">Số lượng</th>
                                                <th class="product-subtotal">Tổng</th>
                                            </tr>

                                        </thead>
                                        <tbody>

                                            @php
                                                $sale = 0;    
                                            @endphp

                                            @foreach($products as $p)
                                            <tr class="woocommerce-cart-form__cart-item cart_item">

                                                <td class="product-remove">
                                                    <a href="{{ route('cartRemoveAll', $p['item']['id']) }}"
                                                        class="remove" aria-label="Xóa sản phẩm này"
                                                        data-product_id="121854" data-product_sku="3337875588829">×</a>
                                                </td>

                                                <td class="product-thumbnail">
                                                    <a
                                                        href="{{ route('chitiet', $p['item']['id']) }}"><img
                                                            width="300" height="300"
                                                            src="uploads/product/{{ $p['item']['image'] }}"
                                                            class="lazy lazy-hidden jetpack-lazy-image jetpack-lazy-image--handled"
                                                            alt="" data-lazy-type="image"
                                                            srcset="uploads/product/{{ $p['item']['image'] }}"
                                                            data-lazy-loaded="1"></a> </td>

                                                <td class="product-name" data-title="Sản phẩm">
                                                    <a
                                                        href="{{ route('chitiet', $p['item']['id']) }}">{{ $p['item']['name'] }}</a>
                                                </td>

                                                <td class="product-price" data-title="Giá">
                                                    <span class="woocommerce-Price-amount amount">@if($p['item']['promotion_price'] != 0) {{ number_format(($p['item']['promotion_price']), 0, ',', '.') }} @else {{ number_format(($p['item']['unit_price']), 0, ',', '.') }} @endif<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span>
                                                </td>

                                                <td class="product-quantity" data-title="Số lượng">

                                                    <div class="quantity">
                                                        <a class="minus" href="{{ route('cartRemoveOne', $p['item']['id']) }}"><input type="button" value="-" class="minus"></a>
                                                        <label class="screen-reader-text"
                                                            for="quantity_5ccac02f77822">Số lượng</label>
                                                        <input type="number" id="quantity_5ccac02f77822"
                                                            class="input-text qty text" step="1" min="0" max=""
                                                            name="cart[1392c6a99eeca063c4bc0de55fc51d71][qty]" value="{{ $p['qty'] }}"
                                                            title="SL" size="4" pattern="[0-9]*" inputmode="numeric"
                                                            aria-labelledby="Kem Dưỡng Da Vichy Aqualia Thermal Rehydrating Light Cream (50ml) số lượng">

                                                            <a class="minus" href="{{ route('cartAdd', $p['item']['id']) }}"><input type="button" value="+" class="plus"></a>
                                                    </div>
                                                </td>

                                                <td class="product-subtotal" data-title="Tổng">
                                                    <span class="woocommerce-Price-amount amount">
                                                        @if($p['item']['promotion_price'] != 0)
                                                        @php
                                                            $sale += $p['qty'] * $p['item']['promotion_price'];
                                                            
                                                        @endphp
                                                            {{ $sum = number_format(( $p['qty'] * $p['item']['promotion_price']), 0, ',', '.') }}<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        @else
                                                        @php
                                                            $sale += $p['qty'] * $p['item']['unit_price'];
                                                            
                                                        @endphp
                                                            {{ $sum =  number_format(( $p['qty'] * $p['item']['unit_price']), 0, ',', '.') }}<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        @endif
                                                </td>
                                            </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>



                            </form>


                        </div>


                        <div class="col-lg-4">

                            <div class="cart-collaterals">
                                <div class="cart_totals ">


                                    <h2>Tổng cộng</h2>

                                    <div class="responsive-table">
                                        <div class="responsive-table">
                                            <table cellspacing="0" class="shop_table shop_table_responsive">

                                                <tbody>
                                                    <tr class="cart-subtotal">
                                                        <th>Tổng thu</th>
                                                        <td data-title="Tổng thu"><span
                                                                class="woocommerce-Price-amount amount">
                                                                @php
                                                                      echo number_format(($sale), 0, ',', '.');
                                                                @endphp₫<span
                                                                    class="woocommerce-Price-currencySymbol"></span></span>
                                                        </td>
                                                    </tr>




                                                    <tr class="woocommerce-shipping-totals shipping">
                                                        <th>Giao hàng</th>
                                                        <td data-title="Giao hàng">
                                                            Giao hàng tiêu chuẩn

                                                        </td>
                                                    </tr>






                                                    <tr class="order-total">
                                                        <th>Tổng</th>
                                                        <td data-title="Tổng"><strong><span
                                                                    class="woocommerce-Price-amount amount">@php
                                                                    echo number_format(($sale), 0, ',', '.');
                                                                    @endphp₫<span
                                                                        class="woocommerce-Price-currencySymbol"></span></span></strong>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="wc-proceed-to-checkout">

                                        <a href="{{ route('thanhtoan') }}"
                                            class="checkout-button button alt wc-forward">
                                            Thanh toán</a>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </article><!-- #post -->



        </div><!-- .site-content -->



    </div> <!-- end row -->
</div>
@endsection('pages/giohang')