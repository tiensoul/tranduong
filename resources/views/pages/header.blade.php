<div class="mobile-nav">
    <img src="wp-content/uploads/2019/04/sale-web.png" class="bhm">
    <div class="woodmart-search-form">
    <form role="search" method="get" id="searchform" class="searchform " action="{{ route('timkiem') }}" data-thumbnail="1"
            data-price="1" data-count="4" data-post_type="product">
            @csrf
            <div>
                <input type="text" class="s suntory-opt-search" data-rid="93" placeholder="Tìm sản phẩm" value="" name="keyword"
                    style="padding-right: 230px;">
                <button type="submit" class="searchsubmit"></button>
                <input type="hidden" name="post_type" class="post_type" value="product">

            </div>
        </form>
        {{-- <div id="matches" class="matches res-matches-93" style=""></div> --}}
    </div>
    <div class="menu-menu-di-dong-container">
        <ul id="menu-menu-di-dong" class="site-mobile-menu">
            <li id="menu-item-70163"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70163 menu-item-design-default item-event-hover">
                <a href="{{ route('trangchu') }}">Trang chủ</a></li>

            @foreach($loaisanpham as $lsp)
            <li id="menu-item-26120"
                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-26120 menu-item-design-default item-event-hover">
                <a href="{{ route('loaisp', [$lsp->id, $lsp->id]) }}">{{ $lsp->name }}</a>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="header-links my-account-with-text">
        <ul>
            @if(Auth::check())
                <li><a href="{{ route('dangxuat') }}">Xin chào, {{ Auth::user()->name }}</a></li>
            @else
                <li class=""><a href="{{ route('taikhoan') }}">Đăng nhập / Đăng ký</a></li>
            @endif
        </ul>
    </div>

    <div class="nutyaccount-wrapper ">
        <div class="nuty-account login-side-opener"><i class="fa fa-user-circle-o"></i><a href="{{ route('taikhoan') }}">Đăng nhập
                <br><small>Tạo tài khoản</small></a>
            </a> </div>
    </div>
    <div class="mobile-nav-info" style="padding: 10px 0">
        <div class="clearfix">
            <i class="icon-earphones-alt icons" style="float: left;font-size: 40px;margin: 0 15px;color: #eb2a8e;"></i>
            <div class="float:left">
                <span style="display: block;"><a href="tel:1900.xxx.xxx"
                        style=" font-size: 25px; vertical-align: middle; line-height: 45px; ">1900.xxx.xxx</a>
            </div>
        </div>
    </div>
</div>
<!--END MOBILE-NAV-->
<div class="cart-widget-side">
    <div class="widget-heading">
        <h3 class="widget-title">Giỏ hàng</h3>
        <a href="#" class="widget-close">đóng</a>
    </div>
    <div class="widget woocommerce widget_shopping_cart">
        <div class="widget_shopping_cart_content"></div>
    </div>
</div>
<div class="website-wrapper">




    <!-- HEADER -->
    <header class="main-header header-has-no-bg header-base icons-design-line color-scheme-dark">

        <div class="container">
            <div class="wrapp-header">
                <div class="site-logo">

                    <div class="mobile-nav-icon">
                        <span class="basel-burger"></span>
                    </div>
                    <!--END MOBILE-NAV-ICON-->

                    <div class="basel-logo-wrap switch-logo-enable">
                        <a href="{{ route('trangchu') }}" class="basel-logo basel-main-logo" rel="home">
                            <img src="uploads/{{ $config->logo_image }}" alt="" /> </a>
                        <a href="{{ route('trangchu') }}" class="basel-logo basel-sticky-logo" rel="home">
                            <img src="uploads/{{ $config->logo_image }}" alt="" /> </a>
                    </div>

                    <div class="header-categories" style="display: none;">
                        <div class="secondary-header">
                            <div class="mega-navigation show-on-hover" role="navigation">
                                <span class="menu-opener"><span class="burger-icon"></span>Danh mục<span
                                        class="arrow-opener"></span></span>
                                <div class="categories-menu-dropdown basel-navigation">
                                    <div class="menu-menu-di-dong-container">
                                        <ul id="menu-menu-di-dong-1" class="menu">
                                            <li
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70163 menu-item-design-default item-event-hover">
                                                <a href="{{ route('trangchu') }}">Trang chủ</a></li>
                                            @foreach($loaisanpham as $lsp)
                                            <li
                                                class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-26120 menu-item-design-default item-event-hover">
                                                <a href="{{ route('loaisp', [$lsp->id, $lsp->id]) }}">{{ $lsp->name }}</a>
                                            </li>
                                            @endforeach
                                            <li
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70163 menu-item-design-default item-event-hover">
                                                <a href="{{ route('trangchu') }}">Sản phẩm khuyến mãi</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="suntory-sticky-search" style="display: none;">
                        <div class="widget woodmart-search-form">
                            <div class="search-extended">
                                <form role="search" method="get" id="searchform" class="searchform " action="{{ route('timkiem') }}"
                                    data-thumbnail="1" data-price="1" data-count="4" data-post_type="product">
                                    @csrf
                                    <div>
                                        <input type="text" class="s suntory-opt-search" data-rid="17" placeholder="Tìm sản phẩm" value=""
                                            name="keyword" style="padding-right: 230px;">
                                        <button type="submit" class="searchsubmit"></button>
                                        <input type="hidden" name="post_type" class="post_type" value="product">

                                    </div>
                                </form>
                                {{-- <div id="matches" class="matches res-matches-17" style=""></div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="header-contact suntory-sticky-header-contact" style="display: none;">
                        <a href="tel:1900.xxx.xxx"
                            onclick="ga('send', 'event', { eventCategory: 'Phone-Call', eventAction: 'Click-to-Call', eventLabel: 'Call-1900.xxx.xxx'});">
                            <div class="header-contact-desc">
                                <h3>1900.xxx.xxx</h3>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="widgetarea-head">
                    <div class="search-button basel-search">
                        <a href="#" class="sun-search-mobile">
                            <i class="fa fa-search"></i>
                        </a>
                        <div class="suntory-search-wrapper">

                            <div class="search-button basel-search woodmart-search-form">
                                <a href="#" class="sun-search-mobile">
                                    <i class="fa fa-search"></i>
                                </a>
                                <div class="suntory-search-wrapper">
                                    <form role="search" method="get" id="searchform" class="searchform " action="{{ route('timkiem') }}"
                                        data-thumbnail="1" data-price="1" data-count="4" data-post_type="product">
                                        @csrf
                                        <div>
                                            <input type="text" class="s suntory-opt-search" data-rid="35" placeholder="Tìm sản phẩm"
                                                value="" name="keyword" style="padding-right: 230px;">
                                            <button type="submit" class="searchsubmit"></button>
                                            <input type="hidden" name="post_type" class="post_type" value="product">

                                        </div>
                                    </form>
                                    {{-- <div id="matches" class="matches res-matches-35" style=""></div> --}}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="right-column">
                    <div class="header-links my-account-with-text">
                        <ul>
                                @if(Auth::check())
                                <li>Xin chào {{ Auth::user()->name }} - <a href="{{ route('dangxuat') }}">Đăng xuất</a></li>
                            @else
                                <li class=""><a href="{{ route('taikhoan') }}">Đăng nhập / Đăng ký</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="shopping-cart basel-cart-design-1">
                        <a href="{{ route('giohang') }}">
                            <div class="">
                                <span class=""><span class=""><span
                                            class=""></span></span></span>Giỏ hàng
                            </div>
                            <span class="cart_items_number counter_number animated rubberBand"> <span
                                    class="basel-cart-number">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
                            </span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="navigation-wrap">
            <div class="container">
                <div class="main-nav site-navigation basel-navigation menu-center" role="navigation">
                    <div class="menu-main-menu-container">
                        <ul id="menu-main-menu" class="menu">
                            <li id="menu-item-66355"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66355 menu-item-design-default item-event-hover">
                                <a href="{{ route('trangchu') }}">Trang chủ</a>
                            </li>
                            @foreach($loaisanpham as $lsp)
                            <li id="menu-item-66355"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66355 menu-item-design-default item-event-hover">
                                <a href="{{ route('loaisp', [$lsp->id, $lsp->id]) }}}">{{ $lsp->name }}</a>
                            </li>
                            @endforeach
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70163 menu-item-design-default item-event-hover">
                                                <a href="{{ route('sanphamkhuyenmai') }}">Sản phẩm khuyến mãi</a></li>
                        </ul>
                    </div>
                </div>
                <!--END MAIN-NAV-->
            </div>
        </div>



    </header>
    <!--END MAIN HEADER-->