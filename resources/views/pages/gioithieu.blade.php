@extends('pages/master')
@section('content')
</div>

<div class="container" style="margin-top: 40px;">
    <div class="row">




        <div class="site-content col-sm-12" role="main">

            <article id="post-294" class="post-294 page type-page status-publish">

                <div class="entry-content">
                    <p>Nuty Cosmetics tự hào là một trong những chuỗi cửa hàng mỹ phẩm lớn và đáng tin cậy nhất tại Sài
                        Gòn, nơi có thể thỏa mãn niềm đam mê trong cuộc chơi phấn son của hàng triệu tín đồ yêu shopping
                        từ Nam ra Bắc. Được ưu ái với tên gọi “Thiên Đường Mỹ Phẩm Triệu Like”, Nuty luôn được xem là
                        mái nhà chung của hàng nghìn mặt hàng mỹ phẩm thuộc rất nhiều thương hiệu lớn nhỏ, hội tụ từ
                        khắp các quốc gia trên thế giới. Nuty luôn sẵn sàng đáp ứng mọi nhu cầu làm đẹp cho phái đẹp lẫn
                        phái mạnh mà không cần phải lo về giá và chất lượng sản phẩm.</p>
                    <p><strong>Tọa Lạc Tại Những Vị Trí Đắc Địa Của Sài Gòn</strong><img
                            class="aligncenter wp-image-50976 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-1024x672.jpg?resize=1024%2C672&amp;ssl=1"
                            alt="" width="1024" height="672" data-recalc-dims="1"
                            srcset="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-1024x672.jpg?resize=1024%2C672&amp;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/1-600x394.jpg 600w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/1-300x197.jpg 300w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-768x504.jpg 768w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/1-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50976 size-large"
                                src="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-1024x672.jpg?resize=1024%2C672&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-1024x672.jpg?resize=1024%2C672&#038;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/1-600x394.jpg 600w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/1-300x197.jpg 300w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1-768x504.jpg 768w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/1-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/1.jpg 1200w"
                                alt="" width="1024" height="672" data-recalc-dims="1" /></noscript>Các chi nhánh của
                        Nuty đều nằm ở vị trí vô cùng thuận lợi cho các bạn học sinh, sinh viên, các cô gái văn phòng và
                        khu vực lân cận tham gia mua sắm. Với những cô nàng Sài Thành thì có thể dễ dàng đến trực tiếp 3
                        chi nhánh của shop để ngắm nghía và sờ tận tay trước khi mua hàng nha.</p>
                    <p><strong>Add 1 (Sỉ):</strong>&nbsp;31/7 Hoàng Việt, P. 4, Q. Tân Bình</p>
                    <p><strong>Add 1:</strong>&nbsp;490 đường 3/2, P.14,quận 10</p>
                    <p><strong>Add 2:</strong>&nbsp;235 Khánh Hội, P.5, Quận 4</p>
                    <p><strong>Add 3:</strong>&nbsp;22 Hoàng Việt (Út Tịch), P.4, Q. Tân Bình</p>
                    <p><strong>Add 4:</strong>&nbsp;19 Út Tịch (22 Hoàng Việt), P.4 Q.Tân Bình</p>
                    <p><strong>Add 5:</strong>&nbsp;455 – 457 Phan Văn Trị, P.5, Q. Gò Vấp</p>
                    <p>Nuty lập hẳn 1 “đại bản doanh” hoành tráng tại&nbsp;<strong>31/7 Hoàng Việt, P.4, Q. Tân
                            Bình</strong>&nbsp;để chuyên phục vụ cho khách sỉ với số lượng mỹ phẩm “khủng” chưa từng có.
                    </p>
                    <p><strong>Không Gian Bài Trí Đơn Giản – “Sang Không Cần Cố” Chỉ Có Tại Nuty Cosmetics</strong><img
                            class="aligncenter wp-image-50977 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i0.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-1024x683.jpg?resize=1024%2C683&amp;ssl=1"
                            alt="" width="1024" height="683" data-recalc-dims="1"
                            srcset="https://i0.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-1024x683.jpg?resize=1024%2C683&amp;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/2-600x400.jpg 600w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/2-300x200.jpg 300w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/2-768x512.jpg 768w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/2.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50977 size-large"
                                src="https://i0.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-1024x683.jpg?resize=1024%2C683&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i0.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-1024x683.jpg?resize=1024%2C683&#038;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/2-600x400.jpg 600w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/2-300x200.jpg 300w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/2-768x512.jpg 768w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/2-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/2.jpg 1200w"
                                alt="" width="1024" height="683" data-recalc-dims="1" /></noscript>Đến với Nuty
                        Cosmetics, chắc chắn bạn sẽ bị ấn tượng với tông màu tím bắt mắt và đậm chất Girly mà shop mong
                        muốn hướng đến tất cả các cô gái yêu làm đẹp. Những chiếc kệ được bày trí hợp lý, xếp ngay ngắn
                        hàng nghìn món đồ mỹ phẩm xinh yêu. Mỗi ngăn, mỗi tầng là một khu vực hàng hóa riêng biệt, được
                        phân theo công dụng như skincare, makeup, chăm sóc tóc, dưỡng thể, nước hoa, thực phẩm chức
                        năng… để các bạn dễ dàng tìm kiếm và mua sắm.</p>
                    <p><strong>Sở Hữu Hàng Ngàn Sản Phẩm Từ High-end Đến Drugstore Giá “Chạm Sàn” &nbsp;</strong><img
                            class="aligncenter wp-image-50978 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i2.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-1024x683.jpg?resize=1024%2C683&amp;ssl=1"
                            alt="" width="1024" height="683" data-recalc-dims="1"
                            srcset="https://i2.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-1024x683.jpg?resize=1024%2C683&amp;ssl=1 1024w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-600x400.jpg 600w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-300x200.jpg 300w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/3-768x512.jpg 768w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/3-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50978 size-large"
                                src="https://i2.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-1024x683.jpg?resize=1024%2C683&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i2.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-1024x683.jpg?resize=1024%2C683&#038;ssl=1 1024w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-600x400.jpg 600w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3-300x200.jpg 300w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/3-768x512.jpg 768w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/3-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/3.jpg 1200w"
                                alt="" width="1024" height="683" data-recalc-dims="1" /></noscript>Nuty tự hào là shop
                        luôn “đi tắt đón đầu”, cập nhật xu hướng mỹ phẩm nhanh nhất để “chiều lòng” tất cả các tín đồ mê
                        làm đẹp. Hàng hóa đa dạng từ Âu sang Á cam kết chính hãng 100%, có bill cho bạn kiểm chứng hoặc
                        bạn có thể thoải mái check code ngay tại shop.</p>
                    <p>Luôn mang đến cho khách hàng mức giá yêu thương nhất nên dù túi tiền của bạn có “eo hẹp” đến đâu
                        cũng sẽ dễ dàng lựa chọn cho mình một sản phẩm “ưng bụng”. Đặc biệt, cho đến thời điểm hiện tại
                        Nuty cũng đã hợp tác thành công và trở thành đại lý phân phối chính thức của 15 “ông trùm” mỹ
                        phẩm đình đám như Paula’s Choice, Vichy, Bioderma, La Roche-Posay, Murad… cùng hàng chục thương
                        hiệu nổi tiếng khác. Cũng nhờ thế mà lượng hàng hóa tại Nuty luôn luôn phong phú, đa dạng đồng
                        thời cam kết đảm bảo uy tín chất lượng.</p>
                    <p><strong>“Bắt Bệnh Kê Đơn” Hoàn Toàn Miễn Phí Với Cỗ Máy Siêu Âm Da</strong><img
                            class="aligncenter wp-image-50979 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-1024x684.jpg?resize=1024%2C684&amp;ssl=1"
                            alt="" width="1024" height="684" data-recalc-dims="1"
                            srcset="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-1024x684.jpg?resize=1024%2C684&amp;ssl=1 1024w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-600x401.jpg 600w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-300x200.jpg 300w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/4-768x513.jpg 768w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/4-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50979 size-large"
                                src="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-1024x684.jpg?resize=1024%2C684&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i1.wp.com/nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-1024x684.jpg?resize=1024%2C684&#038;ssl=1 1024w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-600x401.jpg 600w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4-300x200.jpg 300w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/4-768x513.jpg 768w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/4-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/4.jpg 1200w"
                                alt="" width="1024" height="684" data-recalc-dims="1" /></noscript>Với tiêu chí hướng
                        người tiêu dùng đến vẻ đẹp toàn diện, bất cứ khách hàng nào khi đến mua sắm tại các chi nhánh,
                        đều có cơ hội được trải nghiệm dịch vụ siêu âm da miễn phí để “bắt bệnh” cho làn da của mình. Từ
                        đó shop sẽ “kê đơn” và tư vấn cặn kẽ cách chăm sóc da lẫn chế độ dinh dưỡng chuyên sâu để giải
                        quyết triệt để các vấn đề này sao cho phù hợp nhất với làn da mỗi bạn. Vì vậy đừng ngại mà hại
                        thân, bạn cứ mạnh dạn sử dụng dịch vụ này nhé!</p>
                    <p><strong>Bố Trí Cả Khu Vực Test Sản Phẩm Thả Ga “Không Tốn Một Xu”</strong><img
                            class="aligncenter wp-image-50980 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i2.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/5-1024x684.jpg?resize=1024%2C684&amp;ssl=1"
                            alt="" width="1024" height="684" data-recalc-dims="1"
                            srcset="https://i2.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/5-1024x684.jpg?resize=1024%2C684&amp;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-600x401.jpg 600w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-300x200.jpg 300w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/5-768x513.jpg 768w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/5.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50980 size-large"
                                src="https://i2.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/5-1024x684.jpg?resize=1024%2C684&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i2.wp.com/nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/5-1024x684.jpg?resize=1024%2C684&#038;ssl=1 1024w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-600x401.jpg 600w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-300x200.jpg 300w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/5-768x513.jpg 768w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/5-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/5.jpg 1200w"
                                alt="" width="1024" height="684" data-recalc-dims="1" /></noscript>Còn gì tuyệt hơn khi
                        các bạn được test sản phẩm miễn phí trước khi rinh người tình “trong mộng” của mình về dinh nhỉ.
                        Tại khu vực này, hội cuồng mỹ phẩm sẽ được dịp chiêm ngưỡng tất tần tật những sản phẩm Hot đã và
                        đang “làm mưa làm gió” trong cộng đồng làm đẹp có mặt tại Nuty. Nhờ đó mà các bạn sẽ nhanh chóng
                        tìm được những sản phẩm“hợp cạ” với mình giữa một trời mỹ phẩm nhìn hoa cả mắt.</p>
                    <p><strong>Luôn Có Những Chương Trình Tích Điểm, Sự Kiện, Khuyến Mãi &amp; Give Away “Cực
                            Khủng”</strong><img
                            class="aligncenter wp-image-50981 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i1.wp.com/nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-1024x684.jpg?resize=1024%2C684&amp;ssl=1"
                            alt="" width="1024" height="684" data-recalc-dims="1"
                            srcset="https://i1.wp.com/nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-1024x684.jpg?resize=1024%2C684&amp;ssl=1 1024w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/6-600x401.jpg 600w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/6-300x200.jpg 300w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-768x513.jpg 768w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/6-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/6.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50981 size-large"
                                src="https://i1.wp.com/nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-1024x684.jpg?resize=1024%2C684&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i1.wp.com/nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-1024x684.jpg?resize=1024%2C684&#038;ssl=1 1024w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/6-600x401.jpg 600w, https://nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/6-300x200.jpg 300w, https://nutycosmetics2.r.worldssl.net/wp-content/uploads/2015/06/6-768x513.jpg 768w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/6-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/6.jpg 1200w"
                                alt="" width="1024" height="684" data-recalc-dims="1" /></noscript>Shop luôn mang đến cơ
                        hội tốt nhất để bạn thỏa mãn niềm đam mê làm đẹp khi ví tiền eo hẹp. Không chỉ có vậy, Nuty còn
                        là một chú ong chăm chỉ “thả thính” với các cô nàng bằng những chương trình khuyến mãi, sự kiện,
                        giảm giá, giveaway… cùng hàng ngàn quà tặng ̣hấp dẫn đươc rất nhiều lượt followers, nghe qua là
                        muốn đến shop ngay và luôn để “hốt trọn ổ” rồi.</p>
                    <ul>
                        <li>Chương trình freeship với hóa đơn 400k nội thành Tp.Hồ Chí Minh.</li>
                        <li>Tích lũy điểm thành viên với mỗi 1000k trên hóa đơn khách hàng sẽ đươc tích lũy môt điểm và
                            đến khi đủ 10 điểm tương ứng khách hàng sẽ đươc tăng voucher tri ̣giá 100k cho lần mua hàng
                            sau.</li>
                        <li>Chương trình Giveaway với những phần quà hấp dẫn lên đến 10.000 comment.</li>
                    </ul>
                    <p><strong>Dịch Vụ Tư Vấn &amp; Chăm Sóc Khách Hàng Chuyên Nghiệp</strong><img
                            class="aligncenter wp-image-50982 size-large jetpack-lazy-image jetpack-lazy-image--handled"
                            src="https://i0.wp.com/nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/7-1024x684.jpg?resize=1024%2C684&amp;ssl=1"
                            alt="" width="1024" height="684" data-recalc-dims="1"
                            srcset="https://i0.wp.com/nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/7-1024x684.jpg?resize=1024%2C684&amp;ssl=1 1024w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/7-600x401.jpg 600w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/7-300x200.jpg 300w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7-768x513.jpg 768w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7.jpg 1200w"
                            data-lazy-loaded="1" sizes="(max-width: 1024px) 100vw, 1024px"><noscript><img
                                class="aligncenter wp-image-50982 size-large"
                                src="https://i0.wp.com/nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/7-1024x684.jpg?resize=1024%2C684&#038;ssl=1"
                                sizes="(max-width: 1024px) 100vw, 1024px"
                                srcset="https://i0.wp.com/nutycosmetics4.r.worldssl.net/wp-content/uploads/2015/06/7-1024x684.jpg?resize=1024%2C684&#038;ssl=1 1024w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/7-600x401.jpg 600w, https://nutycosmetics3.r.worldssl.net/wp-content/uploads/2015/06/7-300x200.jpg 300w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7-768x513.jpg 768w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7-30x20.jpg 30w, https://nutycosmetics1.r.worldssl.net/wp-content/uploads/2015/06/7.jpg 1200w"
                                alt="" width="1024" height="684" data-recalc-dims="1" /></noscript>Dù bạn là một cô nàng
                        bánh bèo “chân ướt chân ráo” mới bước vào shop hay đã quen mặt với Nuty nhiều năm thì chắc chắn
                        sẽ có thiện cảm với nụ cười mến khách luôn thường trực, hiện hữu trên đôi môi xinh xắn của đội
                        ngũ nhân viên tại shop. Sự thông thái về kiến thức làm đẹp, chăm sóc da cũng như sự am hiểu về
                        các sản phẩm của nhân viên Nuty Cosmetics luôn khiến khách hàng “nể phục”. Để có được điều tuyệt
                        vời đó, các bạn nhân viên tại shop đều được đào tạo kiến thức làm đẹp dưới sự hướng dẫn cặn kẽ
                        đến từ các chuyên gia đến từ Murad và thường xuyên trải qua những khóa thử thách kiến thức
                        chuyên môn, nghiệp vụ.</p>
                    <p>Cùng với đó là dịch vụ ship hàng nhanh chóng, trao tận tay khách hàng bất chấp mưa gió, bão bùng
                        của các anh chàng shipper đẹp trai và vô cùng lịch sự của Nuty. Hiện tại Nuty cũng đã đầu tư xây
                        dựng Website riêng&nbsp;<a href="https://nutycosmetics.vn/"
                            data-wplink-edit="true">https://nuty.vn/</a>&nbsp;cũng như fanpage Facebook hay hệ thống
                        tổng đài chăm sóc khách hàng 1900 636 737 phục vụ cho nhu cầu mua sắm Online, không để các nàng
                        yêu làm đẹp chịu thiệt thòi vì cách trở địa lý.</p>
                    <p>Thiên đường không nằm đâu xa, thiên đường chính là nơi có thể thỏa mãn được tất cả niềm đam mê
                        của bạn. Vì một tâm hồn đam mê mỹ phẩm và vì bạn là cô gái biết yêu thương bản thân, tại sao bạn
                        không thử một lần ghé chân và khám phá Nuty Cosmetics – Thiên đường trong mơ của hàng triệu cô
                        gái nhỉ?</p>
                </div>


            </article><!-- #post -->



        </div><!-- .site-content -->



    </div> <!-- end row -->
</div>
@endsection('content')