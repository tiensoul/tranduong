@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
          @if(Session::has('thongbao'))
            <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
          @endif

          @if(count($errors) > 0)
            @foreach($errors->all() as $er)
                <div class="alert alert-danger">{{ $er }}</div>
            @endforeach
          @endif
        <h2>Cấu hình tổng quan</h2>
      </div>
    <div class="col-md-6">
        <form action="{{ route('thaylogo') }}" method="post" name="cauhinh" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="exampleInputFile">Tải lên logo cho trang web</label>
                <input type="file" name="logo" required>
            </div>
            <button type="submit" name="submitlogo" class="btn btn-info">Thay logo</button>
        </form>
            <hr>
            <form action="{{ route('thayslide') }}" method="post" name="slide" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                  <label for="exampleInputFile">Số lượng slide hiển thị ngoài trang chủ khách hàng</label>
                  <p class="help-block">Số lượng slide mặc định được hiển thị là 6 slide:</p>
                  <input type="text" class="form-control" name="slide" required>
              </div>
              <button type="submit" name="submitslide" class="btn btn-success">Áp dụng</button>
          </form>
              <hr>
        <form action="{{ route('thaysanpham') }}" method="post" name="sp" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="exampleInputFile">Tùy chọn số lượng sản phẩm hiển thị</label>
                <p class="help-block">Số lượng sản phẩm khuyễn mãi được hiển thị trên trang chủ:</p>
                <input type="text" name="product_sale" class="form-control" required>
            </div>
            <div class="form-group">
                <p class="help-block">Số lượng sản phẩm nổi bật được hiển thị trên trang chủ:</p>
                <input type="text" name="product_top" class="form-control" required>
            </div>
            <button type="submit" name="submit" class="btn btn-danger">Áp dụng</button>
        </form>
    </div>

    <!-- Modal -->
    <div id="modalDelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form action="#"></form>
        <form action="{{ route('xoasanpham') }}" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin</h4>
          </div>
          <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa sản phẩm này</label>
                <input type="hidden" class="form-control" name="idxoa" id="idxoa">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-danger" name="xoasp" value="Xóa sản phẩm">
          </div>
          <form>
        </div>

      </div>
    </div>
    <!-- End-Modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
@endsection