@extends('admin/master')
@section('contentcp')
@if(!Auth::check())
  <script>window.location = "admin/login";</script>
@endif
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        @if(Session::has('thongbao'))
          <div class="alert alert-success">{{ Session::get('thongbao') }}</div>
        @endif

        @if(count($errors) > 0)
          @foreach($errors->all() as $er)
              <div class="alert alert-danger">{{ $er }}</div>
          @endforeach
        @endif
        <h2>Danh sách khách hàng</h2>
      </div>
      <table id="khachhang" class="display table table-bordered table-hover">
       <thead>
          <tr>
             <th>ID</th>
             <th>Tên KH</th>
             <th>Giới tính</th>
             <th>Email</th>
             <th>Địa chỉ</th>
             <th>Số điện thoại</th>
             <th>Thao tác</th>
          </tr>
       </thead>
       <tbody>
        @php
          $i = 1;
        @endphp
        @foreach($customers as $customer)
          <tr>
            <th scope="row">@php echo $i++; @endphp</th>
             <td>{{ $customer->name }}</td>
             <td>@if($customer->gender == 0) Nữ @else Nam @endif</td>
             <td>{{ $customer->email }}</td>
             <td>{{ $customer->address }}</td>
             <td>{{ $customer->phone_number }}</td>
             <td><a class="dt-edit" style="cursor: pointer;">Sửa</a> - <a class="dt-remove" style="cursor: pointer;">Xóa</a></td>
          </tr>
          @endforeach

       </tbody>
      </table>

      <div class="page-header">
      <h2>Danh sách thành viên</h2>
    </div>
    <table id="thanhvien" class="display table table-bordered table-hover">
     <thead>
        <tr>
           <th>ID</th>
           <th>Họ và tên</th>
           <th>Email</th>
           <th>Địa chỉ</th>
           <th>Số điện thoại</th>
           <th>Quyền</th>
           <th>Thao tác</th>
        </tr>
     </thead>
     <tbody>

      @foreach($users as $user)
        <tr>
          <th scope="row">{{ $user->id }}</th>
           <td>{{ $user->name }}</td>
           <td>{{ $user->email }}</td>
           <td>{{ $user->address }}</td>
           <td>{{ $user->phone }}</td>
           <td>@if($user->role == 1) Quản trị @else Người dùng @endif</td>
           <td><a class="dt-edit-member" style="cursor: pointer;">Sửa</a> - <a class="dt-remove-tv" style="cursor: pointer;">Xóa</a></td>
        </tr>
        @endforeach

     </tbody>
    </table>
    </div>

    <!-- Modal -->
    <div id="modalEdit" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <form action="{{ route('capnhatKH') }}" method="POST" id="form" name="capnhatKH" enctype="multipart/form-data" autocomplete="off">
          @csrf
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Thông tin sản phẩm</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <input type="hidden" class="form-control disabled" name="emailedit" id="emailedit">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Tên khách hàng</label>
                <input type="text" class="form-control" name="tenkh" id="tenkh">
              </div>
              <div class="form-group">
                <label for="sel1">Giới tính</label>
                <select name="gioitinh" class="form-control" id="gioitinh">
                  <option value="1" selected>Nam</option>
                  <option value="0">Nữ</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" name="email" id="email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Địa chỉ</label>
                <input type="text" class="form-control" name="diachi" id="diachi">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Số điện thoại</label>
                <input type="text" class="form-control" name="sodienthoai" id="sodienthoai">
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <button type="submit" class="btn btn-success" name="submit">Cập nhật</button>
          </div>
          
        </div>

      </div>
    </div>
    <!-- End-Modal -->

    

      <!-- Modal -->
    <div id="modalEditMember" class="modal fade" role="dialog">

        <div class="modal-dialog">
  
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Thông tin thành viên</h4>
            </div>
            <div class="modal-body">
               
              <div class="form-group">
                  <input type="hidden" class="form-control disabled" name="ideditmember" id="ideditmember">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Họ và tên</label>
                  <input type="text" class="form-control" name="tenkhmember" id="tenkhmember">
                </div>
                <div class="form-group">
                  <label for="sel1">Giới tính</label>
                  <select name="gioitinhmember" class="form-control" id="gioitinhmember">
                    <option value="1" selected>Nam</option>
                    <option value="0">Nữ</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="text" class="form-control" name="emailmember" id="emailmember">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Địa chỉ</label>
                  <input type="text" class="form-control" name="diachimember" id="diachimember">
                </div>
                <div class="form-group">
                  <label for="sel1">Quyền</label>
                  <select name="quyenmember" class="form-control" id="quyenmember">
                    <option value="1" selected>Quản trị</option>
                    <option value="0">Người dùng</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Số điện thoại</label>
                  <input type="text" class="form-control" name="sodienthoaimember" id="sodienthoaimember">
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
              <button type="submit" class="btn btn-success" name="submitmember">Cập nhật thông tin</button>
            </div>
            
          </div>
  
        </div>
      </div>
      <!-- End-Modal -->
  
      <!-- Modal -->
      <div id="modalDelete" class="modal fade" role="dialog">
        <div class="modal-dialog">
  
          <!-- Modal content-->
          <div class="modal-content">
              
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Thông tin</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa khách hàng này?</label>
                  <input type="hidden" class="form-control" name="emailxoa" id="emailxoa">
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
              <button type="submit" class="btn btn-danger" name="xoakh">Xóa khách hàng</button>
            </div>
            
          </div>
          </div>
        </div>
      </div>
      <!-- End-Modal -->

      <!-- Modal -->
      <div id="modalDelete1" class="modal fade" role="dialog">
        <div class="modal-dialog">
  
          <!-- Modal content-->
          <div class="modal-content">
              
            
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Thông tin</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Bạn chắc chắn muốn xóa thành viên này?</label>
                  <input type="hidden" class="form-control" name="emaixoatv" id="emailxoatv">
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-default" data-dismiss="modal">Đóng</button>
              <button type="submit" class="btn btn-danger" name="xoatv" id="xoatv">Xóa thành viên</button>
            </div>
          </form>
          </div>
  
        </div>
      </div>
      <!-- End-Modal -->
  
  

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="sourceadmin/js/jquery-3.3.1.js"></script>
    <script src="sourceadmin/js/jquery.dataTables.min.js"></script>
    <script src="sourceadmin/js/dataTables.bootstrap.min.js"></script>
    <script src="sourceadmin/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#khachhang').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
    <script>
      $(document).ready(function() {
        $('#thanhvien').DataTable({
          "pagingType": "full_numbers",
          "language": {
              "search": "Tìm kiếm:",
          }
        });
      });
    </script>
    <script>
        //Edit row buttons
        $('.dt-edit').each(function () {
          $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
              $('#emailedit').val(dtRow[0].cells[3].innerHTML);
              $('#tenkh').val(dtRow[0].cells[1].innerHTML);
              console.log(dtRow[0].cells[2].innerHTML);
              //$('#gioitinh').val(dtRow[0].cells[2].innerHTML);
              //document.getElementById("gioitinh").value = dtRow[0].cells[2].innerHTML;
              $('#email').val(dtRow[0].cells[3].innerHTML);
              $('#diachi').val(dtRow[0].cells[4].innerHTML);
              $('#sodienthoai').val(dtRow[0].cells[5].innerHTML);
            }
            $('#modalEdit').modal('show');
          });
        });
  
        //add
        $('.dt-add').each(function () {
          $(this).on('click', function(evt){
            $('#modalAdd').modal('show');
          });
        });
  
         //delete
        $('.dt-remove').each(function () {
          $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
              $('#emailxoa').val(dtRow[0].cells[3].innerHTML);
            }
            $('#modalDelete').modal('show');
          });
        });

         //delete
        $('.dt-remove-tv').each(function () {
          $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
              $('#emailxoatv').val(dtRow[0].cells[0].innerHTML);
            }
            $('#modalDelete1').modal('show');
          });
        });
      </script>

      <script>
          //Edit row buttons
          $('.dt-edit-member').each(function () {
            $(this).on('click', function(evt){
              $this = $(this);
              var dtRow = $this.parents('tr');
              for(var i=0; i < dtRow[0].cells.length; i++){
                $('#ideditmember').val(dtRow[0].cells[0].innerHTML);
                $('#tenkhmember').val(dtRow[0].cells[1].innerHTML);
                console.log(dtRow[0].cells[2].innerHTML);
                //$('#gioitinh').val(dtRow[0].cells[2].innerHTML);
                //document.getElementById("gioitinh").value = dtRow[0].cells[2].innerHTML;
                $('#emailmember').val(dtRow[0].cells[2].innerHTML);
                $('#diachimember').val(dtRow[0].cells[3].innerHTML);
                $('#sodienthoaimember').val(dtRow[0].cells[4].innerHTML);
              }
              $('#modalEditMember').modal('show');
            });
          });
        </script>
  
@endsection